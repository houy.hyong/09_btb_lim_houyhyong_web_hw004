import React, { cloneElement, Component } from "react";
import UserInfoTable from "./UserInfoTable";

export default class UserInput extends Component {
    //----------------------------
    constructor() {
        super();
        this.state = {
        userInfo: [
            // {
            //   id: "1",
            //   userEmail: "houyhyong@gmail.com",
            //   userName: "houyhyong",
            //   userAge: 22,
            // },
            // {
            //     id: "",
            //     userEmail: "",
            //     userName: "",
            //     userAge: "",
            //   },
        ],
        newEmail: "",
        newName: "",
        newAge: "",
        newStatus: "Pending",
        };
    }
    //----------------------------

    //--------get Email Name Age
    handleEmail = (event) => {
        // console.log(event.target.value);
        this.setState({
        newEmail: event.target.value,
        });
    };

    handleName = (event) => {
        // console.log(name)
        this.setState({
        newName: event.target.value,
        });
    };

    handleAge = (event) => {
        // console.log(age)
        this.setState({
        newAge: event.target.value,
        });
    };
    //---------------------------------

    //-----    function when click on Register button   ----------
    onSubmit = () => {
        // console.log(this.state.userInfo.length + 1)
        const newUserInfo = {
            id: this.state.userInfo.length + 1,
            userEmail: this.state.newEmail,
            userName: this.state.newName,
            userAge: this.state.newAge,
            status: "Pending"
        };

        //update new array
        this.setState(
        {
            userInfo: [...this.state.userInfo, newUserInfo],
            // newEmail: "",
            // newName: "",
            // newAge: "",
        }
        // ()=>console.log("New Array" , this.state.userInfo)
        );
    };
    //-------------------------------------------

    //----- button Action - Pending / Done
    pendingDoneButton = (id) => {
        this.state.userInfo.map((info) => {
        if (info.id == id) {
            info.status = info.status === "Pending" ? "Done" : "Pending";
        }
        });
        this.setState({});
    };

    render() {
        return (
           
            <div class="block justify-center mx-auto w-3/4 font-[Viga] ">
                
                {/*---------  INPUT INFO    ---------*/}
                <div>
                    {/* -----   Title   ----- */}
                    <p class="underline underline-offset-8 text-2xl mx-auto flex pl-0 pr-0 justify-center my-6 text-[#4D3239] sm:flex sm:justify-center sm:my-9 md:flex md:text-3xl lg:text-4xl lg:my-16 lg:flex lg:justify-center xl:text-5xl">
                        Please Fill Your Information
                    </p>

                    {/* -----   Email   ----- */}

                </div>
                
                

                
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium lg:text-lg">Your Email</label>
                    
                    <div class="flex">
                        <span class="inline-flex text-sm text-gray-900 bg-gray-200 border-0 border-r-0 border-gray-200 rounded-l-md items-center px-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                            <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                            </svg>
                        </span>
                        
                        <input
                        type="text"
                        id="email-address-icon"
                        class="bg-gray-50 border-0 border-gray-200 text-gray-900 text-sm rounded-none focus:ring-blue-500 focus:border-blue-500 block w-full rounded-r-lg rounded-l-none p-2.5"
                        placeholder="houyhyong@gmail.com"
                        onChange={this.handleEmail}
                        />
                    </div>
                </div>


                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium lg:text-lg">Username</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-200 rounded-l-md">
                        @
                        </span>
                        <input
                        type="text"
                        id="website-admin"
                        class="rounded-none rounded-r-lg bg-gray-50 border border-gray-200 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5"
                        placeholder="houyhyong"
                        onChange={this.handleName}
                        />
                    </div>
                </div>


                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 lg:text-lg">
                        Age
                    </label>
                    <div class="flex">
                        <span class="inline-flex items-center text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-200 rounded-l-md px-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-moon-stars-fill" viewBox="0 0 16 16">
                            <path d="M6 .278a.768.768 0 0 1 .08.858 7.208 7.208 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277.527 0 1.04-.055 1.533-.16a.787.787 0 0 1 .81.316.733.733 0 0 1-.031.893A8.349 8.349 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.752.752 0 0 1 6 .278z"/>
                            <path d="M10.794 3.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387a1.734 1.734 0 0 0-1.097 1.097l-.387 1.162a.217.217 0 0 1-.412 0l-.387-1.162A1.734 1.734 0 0 0 9.31 6.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387a1.734 1.734 0 0 0 1.097-1.097l.387-1.162zM13.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.156 1.156 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.156 1.156 0 0 0-.732-.732l-.774-.258a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732L13.863.1z"/>
                            </svg>
                        </span>
                        <input
                        type="text"
                        id="website-admin"
                        class="rounded-none rounded-r-lg bg-gray-50 border border-gray-200 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5"
                        placeholder="22"
                        onChange={this.handleAge}
                        />
                    </div>
                </div>


                <div class="flex justify-center">
                    <button
                        type="submit"
                        class="text-white bg-[#4D3239] hover:bg-[#BB828B] focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-auto px-5 py-2.5 text-center my-2"
                        onClick={this.onSubmit}
                    >
                        Register
                    </button>
                </div>

                <UserInfoTable 
                    userData={this.state.userInfo}
                    pendingDoneButton={this.pendingDoneButton}
                />
            </div>
            
        );
    }
}
