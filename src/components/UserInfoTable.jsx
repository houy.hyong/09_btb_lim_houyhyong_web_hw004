import React, { Component } from "react";
import Swal from "sweetalert2";
import "animate.css";

export default class UserInfoTable extends Component {
    showMore = (info) => {
        if (info === "null" ? ` ` : info) {
            Swal.fire({
                html:
                    "<b> ID: " +
                    info.id +
                    "<br> Email: " +
                    info.userEmail +
                    "<br> Username: " +
                    info.userName +
                    "<br> Age:" +
                    info.userAge +
                    "</b>",
                showClass: {
                    popup: "animate__animated animate__fadeInDown",
                },
                hideClass: {
                    popup: "animate__animated animate__fadeOutUp",
                },
            });
        }
    };

    render() {

        return (
            <div class="mt-9 mb-0 bg-white">
                <div class="relative overflow-x-auto mb-80">
                    <div class="w-full text-sm text-left text-gray-500">
                        
                        {/* Title */}
                        <div class="text-[#4D3239] uppercase">
                            <div class="grid grid-cols-12  h-12 content-center items-center text-xs  lg:text-lg md:text-sm sm:text-xs">
                                <label scope="col" class="self-center justify-self-center">
                                    ID
                                </label>
                                <label scope="col" class="col-span-4 self-center justify-self-center">
                                    EMAIL
                                </label>
                                <label scope="col" class="col-span-2 self-center justify-self-center">
                                    USERNAME
                                </label>
                                <label scope="col" class="self-center justify-self-center">
                                    AGE
                                </label>
                                <label class="col-span-4 self-center justify-self-center" >ACTION</label>
                            </div>
                        </div>

                        {/* Loop Info */}
                        <div class="text-[#4D3239] uppercase">
                            {this.props.userData.map((info) => (
                                
                                <div key={info.id} class="odd:bg-white-600 even:bg-[#F0D7D3] grid grid-cols-12  h-12 content-center items-center text-xs  lg:text-lg md:text-sm sm:text-xs">
                                                                        
                                    <label scope="col" class="self-center justify-self-center">{info.id}</label>
                                    
                                    <label scope="col" class="col-span-4 self-center justify-self-center">
                                        {info.userEmail === "" ? "null" : info.userEmail}
                                    </label>
                                    
                                    <label scope="col" class="col-span-2 self-center justify-self-center">
                                        {info.userName === "" ? "null" : info.userName}
                                    </label>
                                    
                                    <label scope="col" class="self-center justify-self-center">
                                        {info.userAge === "" ? "null" : info.userAge}
                                    </label>
                                    
                                    <label class="col-span-2 self-center justify-self-center" >
                                        <button
                                            onClick={() => this.props.pendingDoneButton(info.id)}
                                            className={`${info.status == "Pending"
                                                    ? `w-28 focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-2.5 mr-2 mb-0 inline-flex justify-center items-center content-center py-2`
                                                    : `w-28 focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-2.5 mr-2 mb-0 inline-flex justify-center items-center content-center py-2`
                                                }`}
                                        >                    
                                            {info.status}
                                        </button>
                                    </label>

                                    <label class="col-span-2 self-center justify-self-center" >
                                        <button
                                            type="submit"
                                            class="w-28 text-white bg-[#4E8F8D] hover:bg-[#243030] focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto sm:w-auto mr-0 mb-0 inline-flex justify-center items-center content-center py-2 px-5"
                                            onClick={() => this.showMore(info)}
                                        >
                                            Show More
                                        </button>
                                    </label>

                                </div>
                            ))}
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
