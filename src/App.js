import logo from './logo.svg';
import './App.css';
import UserInput from './components/UserInput';


function App() {
  return (
    <div>
      <UserInput/>
    </div>
  );
}

export default App;
